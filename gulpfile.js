'use strict';

var gulp 			= require('gulp'),
	uglify 			= require('gulp-uglify'),
	rename 			= require('gulp-rename'),
	sass 			= require('gulp-sass'),
	maps 			= require('gulp-sourcemaps'),
	autoprefixer 	= require('gulp-autoprefixer'),
	browserSync 	= require('browser-sync'),
	del 			= require('del'),
	jade 			= require('gulp-jade');


gulp.task("compileSass", function () {
	return gulp.src('scss/main.scss')
	.pipe(maps.init())
	.pipe(sass({ includePaths : ['sass/'] }))
	.pipe(autoprefixer())
	.pipe(maps.write('./'))
	.pipe(gulp.dest('css'))
	// .pipe(connect.reload());
	.pipe(browserSync.reload({stream:true}));
});

gulp.task("compileHtml", function () {
	return gulp.src('jade/*.jade')
	.pipe(jade({ pretty: true }))
	.pipe(gulp.dest("./"))
	.pipe(browserSync.reload({stream:true}))
})

gulp.task("watch", function () {
	gulp.watch("scss/**/*.scss", ["compileSass"]);
	gulp.watch("jade/**/*.jade", ["compileHtml"]);
});


gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: "./"
    }
  });
});


gulp.task('start', ['browser-sync', 'watch']);

gulp.task("clean", function (){
	del(["dist", "css/main.css*"]);
});

gulp.task("build", ["compileSass", "compileHtml"], function () {
	gulp.src(["css/**", "scss/**", "js", "index.html", "fonts/**", "img/**"], { base: "./" })
		.pipe(gulp.dest("dist"));
});

gulp.task("default", ["clean"], function () {
	gulp.start("build");
});
